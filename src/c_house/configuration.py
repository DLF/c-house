class Configuration:
    # material
    nut_hull_circle_d = 4.8
    switch_hole_width = 14.2
    screw_head_d = 3.3
    switch_plate_thickness = 1.5
    rim_plate_thickness = 1.0
    cover_plate_thickness = 1.0
    bottom_plate_thickness = 1.0
    nut_height = 1.2
    encoder_shaft_hole_diameter = 7.51

    # part geometry
    pcb_thickness = 1.55

    pcb_switch_plate_distance = 3.5
    screw_hole_diameter = 2.4
    screw_hole_diameter_extended = 2.6
    screw_hole_segments = 40

    # general design
    reset_switch_cutout_width = 4.5 + 1 + 1.5
    controller_cover_cutout_radius = 79
    controller_cover_overlap = 2.4
    border_rounding_radius = 0.5
    outer_wall_width = 1.4
    pcb_bottom_plate_distance = 3  # 3.2


class AtheneConfiguration(Configuration):
    # wall
    outer_wall_width = 0.4 * 3 + 1  # (+ clearance)
    inner_wall_width = 2.8
    extra_outer_wall_width__outer = 3.4
    extra_outer_wall_width__inner = 3.2
    pcb_horizontal_clearance = 0.5
    pcb_vertical_clearance = 0.35

    # specific geometry
    screw_distance = 2.4
    controller_cover_elevation = 5.5

    # switch cutouts for the rim plate
    rim_cutout1_dim = (16, 16.5)
    rim_cutout2_dim = (5, 20)

    # overrides
    pcb_bottom_plate_distance = 2.0  # the .05 balances out the PCB-thickness


class FifooConfiguration(Configuration):

    # heights and thicknesses
    switch_plate_top_plate_distance = 1.6
    top_plate_cover_plate_distance = 1.6

    # wall
    inner_wall_width = 2.8
    pcb_horizontal_clearance = 0.2
    pcb_vertical_clearance = 0.2

    # specific geometry
    spacer_feet_height = 0.2


class FirstTryConfiguration(Configuration):
    outer_wall_width = 4.4
    border_rounding_radius = 1
    switch_plate_top_plate_distance = 1.6
    top_plate_cover_plate_distance = 1.6
    wall_to_pcb_clearance = 0.4
    spacer_feet_height = 0.2
    reset_switch_cutout_width = 4.5 + 1 + 1.5

    # specific geometry
    screw_distance = 2.4

