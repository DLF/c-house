from solid import (
    OpenSCADObject,
    offset,
    linear_extrude,
    translate,
    intersection,
    rotate,
    color,
    square,
    circle
)
from solid.utils import (
    down,
    up
)

from terminal_c import scad_part_models, Vector
from terminal_c import geometry
from terminal_c.configuration.default import Configuration as TermCConfig

from c_house.configuration import FirstTryConfiguration
from c_house.lib.board import OldWallConceptBoard
from c_house import StackedModel, Renderer


class FirstTryBoard(OldWallConceptBoard):

    def __init__(self, configuration: FirstTryConfiguration, board: geometry.Board):
        OldWallConceptBoard.__init__(self, configuration, board)
        self.c = configuration

    @property
    def switch_plate_2d(self) -> OpenSCADObject:
        return (
            self.wall_outline_2d
            -
            self.get_wall_screws_holes(extended_diameter=False)
            -
            self.get_controller_section_cover_area_2d(inner_overlap=True)
            +
            self.switch_plate_controller_screw_nose_2d
            -
            self.switch_holes
            -
            self.switch_field_screw_holes
            -
            self.controller_field_screw_holes
        )

    @property
    def bottom_plate_2d(self) -> OpenSCADObject:
        return super(FirstTryBoard, self).bottom_plate_2d - self.get_wall_screws_holes(extended_diameter=False)

    @property
    def wall_screw_positions(self) -> list[Vector]:
        o = self._board.left_board.pcb_outline
        d = self.c.screw_distance
        usb = self.usb_plug_entry_point[:2]
        jack = Vector.from_tuple(self.jack_entry_point[:2])
        return [
            o[0] + Vector(0, -d),
            o[3] + Vector(-d, -(o[4] - o[3]).length / 1.5),
            o[6] + (o[7] - o[6])/2.0 + Vector(0, -d),
            o[10] + Vector(+d * 1.2, -d),
            o[11] + Vector(0, -d),
            jack + Vector(d, 7),
            o[13] + Vector(0, -d).rotate(self._board.left_board.thumb.cells[-1].angle),
            o[16] + Vector(0, d).rotate(self._board.left_board.thumb.cells[1].angle),
            o[19] + Vector(0, d).rotate(self._board.left_board.thumb.cells[0].angle),
            o[24] + ((o[23]-o[24])/2) + Vector(-d, 0).rotate((o[23]-o[24]).angle),
            o[25] + ((o[26]-o[25])/1) + Vector(d, 0).rotate((o[23]-o[24]).angle),
            o[30] + Vector(0, +d),
            ]

    def get_wall_screws_holes(self, extended_diameter: bool = False) -> OpenSCADObject:
        diameter = (
            self.c.screw_hole_diameter_extended
            if extended_diameter else
            self.c.screw_hole_diameter
        )
        return sum([
            translate(p.as_tuple)(
                circle(d=diameter, segments=self.c.screw_hole_segments)
            )
            for p in self.wall_screw_positions
        ])

    @property
    def wall_segment_bottom(self) -> OpenSCADObject:
        wall_2d = (
                self.wall_outline_2d
                -
                offset(delta=self.c.wall_to_pcb_clearance)(
                    self.pcb_polygon_2d
                )
                -
                self.get_wall_screws_holes()
        )
        return (
                up(self.c.bottom_plate_thickness)(
                    linear_extrude(self.space_between_bottom_and_switch_plate)(
                        wall_2d
                    )
                    +
                    up(self.space_between_bottom_and_switch_plate)(
                        linear_extrude(self.c.switch_plate_thickness)(
                            intersection()(
                                wall_2d,
                                self.get_controller_section_cover_area_2d(inner_overlap=True)
                            )
                        )
                    )
                )
                -
                self.usb_plug
                -
                self.jack_open_upwards
        )

    @property
    def model(self) -> OpenSCADObject:
        c = self.c
        z = 0
        return StackedModel([
            (self.bottom_plate_3d, z, "DodgerBlue"),
            (self.wall_segment_bottom, z, "Grey"),
            (self.pcb_3d, z := z + c.bottom_plate_thickness + c.pcb_bottom_plate_distance, "orange"),
            (self.switches_model, z := z + c.pcb_thickness, "red"),
            (self.promic_model, z, "red"),
            #(self.upper_wall, z, "blue"),
            #(self.controller_wall, z, "pink"),
            (self.jack_model, z, "red"),
            (self.encoder_model, z, "red"),
            (self.switch_plate_3d, z := z + c.pcb_switch_plate_distance, "Grey"),
            #(self.top_plate_3d, z := z + c.switch_plate_top_plate_distance + c.switch_plate_thickness, "hotpink"),
            #(self.controller_cover_plate_3d, z := z + c.top_plate_thickness + c.top_plate_cover_plate_distance, "green")
        ])(strech=6)


if __name__ == "__main__":
    c = FirstTryConfiguration()
    pcb_board = geometry.Board(TermCConfig())
    board = FirstTryBoard(c, pcb_board)
    r = Renderer("firsttry")

    r("switch_plate_2d", board.switch_plate_2d)
    r("bottom_plate_2d", board.bottom_plate_2d)
    r("switch_plate_3d", board.switch_plate_3d)
    r("bottom_plate_3d", board.bottom_plate_3d)

    r("bottom_wall", board.wall_segment_bottom)

    r("model", board.model)
