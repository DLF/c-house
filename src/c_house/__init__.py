import os
import pathlib

from solid import (
    scad_render_to_file,
    OpenSCADObject,
    color,
    mirror
)
from solid.utils import up

_this_path = pathlib.Path(__file__).parent.resolve()


class Renderer:
    def __init__(self, prefix: str, flip: bool):
        self.prefix = prefix
        self.outpath = pathlib.Path(os.path.join(_this_path, "../..", "scad", prefix)).resolve()
        self.flip = flip
        if not os.path.exists(self.outpath):
            os.makedirs(self.outpath)

    def __call__(self, name: str, obj: OpenSCADObject):
        outfile = str(self.outpath) + "/" + name + ".scad"
        print("Render {}".format(outfile))
        scad_render_to_file(mirror((1, 0, 0))(obj) if self.flip else obj, outfile)


class StackedModel:
    def __init__(self, elements: list[tuple[OpenSCADObject, int, str]]):
        self.elements = elements

    def __call__(self, strech: float = 1):
        return sum(
            up(e[1] * strech)(
                color(e[2])(
                    e[0]
                )
            )
            for e in self.elements
        )
