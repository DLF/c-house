from solid import (
    OpenSCADObject,
    offset,
    linear_extrude,
    translate,
    rotate,
    color,
    square,
    circle
)
from solid.utils import (
    down,
    up
)

from terminal_c import scad_part_models
from terminal_c import geometry
from terminal_c.configuration.default import Configuration as TermCConfig

from c_house.configuration import FifooConfiguration
from c_house.lib.board import OldWallConceptBoard
from c_house import StackedModel, Renderer


class FifooBoard(OldWallConceptBoard):
    def __init__(self, configuration: FifooConfiguration, board: geometry.Board):
        OldWallConceptBoard.__init__(self, configuration, board)
        self.c = configuration

    @property
    def inner_wall_2d(self) -> OpenSCADObject:
        c = self.c
        return (
                self.wall_outline_2d
                -
                offset(delta=-c.inner_wall_width)(
                    self.pcb_polygon_2d
                )
        )

    @property
    def lower_wall(self) -> OpenSCADObject:
        c = self.c
        upper_wall_2d = self.outer_wall_2d
        lower_wall_2d = self.inner_wall_2d
        return (
                up(c.bottom_plate_thickness)(
                    linear_extrude(c.pcb_bottom_plate_distance)(
                        lower_wall_2d
                    )
                    +
                    linear_extrude(c.pcb_bottom_plate_distance + c.pcb_thickness + c.pcb_vertical_clearance)(
                        upper_wall_2d
                    )
                )
                -
                self.usb_plug
                -
                self.jack_open_upwards
        )

    @property
    def controller_wall(self):
        controller_wall_height = 10
        controller_wall_thickness = 8
        controller_top_thickness = 0.8
        c = self.c
        feet_height = c.spacer_feet_height
        return (
                up(feet_height)(
                    linear_extrude(controller_wall_height - controller_top_thickness)(
                        self.get_controller_section_cover_area_2d(inner_overlap=False)
                        -
                        offset(delta=-controller_wall_thickness)(
                            self.get_controller_section_cover_area_2d(inner_overlap=False)
                        )
                    )
                    +
                    up(controller_wall_height - controller_top_thickness)(
                        linear_extrude(controller_top_thickness)(
                            self.get_controller_section_cover_area_2d(inner_overlap=False)
                        )
                    )
                )
                -
                linear_extrude(c.pcb_switch_plate_distance)(
                    self.wall_outline_2d
                    -
                    self.get_controller_section_cover_area_2d(inner_overlap=True)
                    +
                    offset(delta=0.2)(self.switch_plate_controller_screw_nose_2d)
                )
                -
                translate(self._board.left_board.controler_section.promic.pos.as_tuple)(
                    color("red")(scad_part_models.Promic()())
                )
                -
                translate(self._board.left_board.controler_section.jack.pos.as_tuple)(
                    color("red")(scad_part_models.Jack()())
                )
                -
                down(0.1)(linear_extrude(20)(
                    self.controller_field_screw_holes
                ))
        )

    @property
    def upper_wall(self) -> OpenSCADObject:
        c = self.c
        height = c.pcb_switch_plate_distance
        feet_height = c.spacer_feet_height
        switch_holes = sum([
            translate(cell.position.as_tuple)(
                rotate(cell.angle)(
                    square((15, 16.5), center=True)
                )
            )
            for cell in self.switch_cells
        ])
        screw_holes = sum([
            translate(pos)(
                circle(d=c.screw_hole_diameter_extended, segments=50)
            )
            for pos in self.switch_screw_positions + self.controller_screw_positions[1:]
        ])
        spacer_feed = sum([
            translate(pos)(
                circle(d=4.4, segments=50)
            )
            for pos in self.switch_screw_positions + self.controller_screw_positions[1:]
        ])
        return (
                up(feet_height)(
                    linear_extrude(height - feet_height)(
                        self.wall_outline_2d
                        -
                        self.switch_field_screw_holes
                        -
                        self.get_controller_section_cover_area_2d(inner_overlap=True)
                        +
                        self.switch_plate_controller_screw_nose_2d
                        -
                        switch_holes
                        -
                        screw_holes
                    )
                )
                +
                linear_extrude(feet_height)(
                    self.outer_wall_2d
                    +
                    spacer_feed
                    -
                    screw_holes
                    -
                    self.get_controller_section_cover_area_2d(inner_overlap=True)
                )
        )

    @property
    def model(self) -> OpenSCADObject:
        c = self.c
        z = 0
        return StackedModel([
            (self.bottom_plate_3d, z, "DodgerBlue"),
            (self.lower_wall, z, "Grey"),
            (self.pcb_3d, z := z + c.bottom_plate_thickness + c.pcb_bottom_plate_distance, "orange"),
            (self.switches_model, z := z + c.pcb_thickness, "red"),
            (self.promic_model, z, "red"),
            (self.upper_wall, z, "blue"),
            (self.controller_wall, z, "pink"),
            (self.jack_model, z, "red"),
            (self.encoder_model, z, "red"),
            (self.switch_plate_3d, z := z + c.pcb_switch_plate_distance, "Grey"),
            #                 up(c.switch_plate_top_plate_distance + c.switch_plate_thickness)(
            #                     # color("hotpink")(self.top_plate_3d)
            #                     # +
            #                     up(c.top_plate_cover_plate_distance + c.top_plate_thickness)(
            #                         # color("green")(self.controller_cover_plate_3d)
            #                     )
            #                 )
            #             )

        ])()


if __name__ == "__main__":
    c = FifooConfiguration()
    pcb_board = geometry.Board(TermCConfig())
    board = FifooBoard(c, pcb_board)
    r = Renderer("fifoo")

    r("switch_plate_2d", board.switch_plate_2d)
    r("controler_wall", board.controller_wall)
    r("model", board.model)

    # scad_render_to_file(scad_board.switch_plate_3d, os.path.join(scad_path, "switch_plate_3d.scad"))
    # scad_render_to_file(scad_board.big_bottom_plate_3d, os.path.join(scad_path, "bottom_plate_3d.scad"))
    # scad_render_to_file(
    #     scad_board.wall_segment_bottom, os.path.join(scad_path, "wall_bottom_right.scad")
    # )
    # scad_render_to_file(
    #     mirror((0, 1, 0))(scad_board.wall_segment_bottom), os.path.join(scad_path, "wall_bottom_left.scad")
    # )
    # scad_render_to_file(
    #     scad_board.switch_plate_spacer, os.path.join(scad_path, "switch_plate_spacer_right.scad")
    # )
    # scad_render_to_file(
    #     mirror((0, 1, 0))(scad_board.switch_plate_spacer), os.path.join(scad_path, "switch_plate_spacer_left.scad")
    # )
