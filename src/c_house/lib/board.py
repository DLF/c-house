from solid import (
    polygon,
    square,
    circle,
    translate,
    rotate,
    mirror,
    linear_extrude,
    offset,
    color,
    intersection,
    cube,
    cylinder,
    OpenSCADObject
)

from solid.utils import (
    down,
    up,
    back,
    forward,
    left,
    right
)

from terminal_c import (
    MagicNumbers as m,
    geometry,
    Polygon,
    Vector,
)

from terminal_c.geometry import SwitchDiodeArrangement

from terminal_c import scad_part_models

from c_house.configuration import Configuration

wall_width = 1.2


def factor_polygon(p: Polygon) -> polygon:
    return polygon(
        points=[v.as_tuple for v in p.points],
    )


class Board:
    def __init__(self, configuration: Configuration, board: geometry.Board):
        self.c = configuration
        self._board = board
        c = self.c

        self.pcb_to_switch_plate_height = (
                c.pcb_thickness
                +
                c.pcb_switch_plate_distance
                +
                c.switch_plate_thickness
        )

        self.bottom_to_switch_plate_height = (
                c.bottom_plate_thickness
                +
                c.pcb_bottom_plate_distance
                +
                self.pcb_to_switch_plate_height
        )

        self.space_between_bottom_and_switch_plate = (
                c.pcb_thickness
                +
                c.pcb_switch_plate_distance
                +
                c.pcb_bottom_plate_distance
        )

        self.z_level_pcb_front = (
                c.bottom_plate_thickness
                +
                c.pcb_bottom_plate_distance
                +
                c.pcb_thickness
        )

        print("Bottom-Switch-plate-spacing: {}".format(self.space_between_bottom_and_switch_plate))
        print("Bottom-Switch-plate-thickness: {}".format(self.bottom_to_switch_plate_height))
        print("PCB-Switch-plate-thickness: {}".format(self.pcb_to_switch_plate_height))
        print("Nut needs 1.55 mm".format(self.bottom_to_switch_plate_height))

    @property
    def switch_screw_positions(self) -> list[tuple]:
        """
        Positions of the screws on the switch plate (finger and thumb section).
        """
        return [c.pos.as_tuple for c in self._board.left_holes.switch_field_screws]

    @property
    def controller_screw_positions(self) -> list[tuple]:
        """
        Positions of the screws in the controller section.
        """
        return [c.pos.as_tuple for c in self._board.left_holes.controller_field_screws]

    @property
    def switch_field_screw_holes(self) -> OpenSCADObject:
        return sum(
            [
                translate(pos)(
                    circle(d=self.c.screw_hole_diameter, segments=60)
                )
                for pos in self.switch_screw_positions
            ]
        )

    @property
    def controller_field_screw_holes(self):
        return sum(
            [
                translate(pos)(
                    circle(d=self.c.screw_hole_diameter, segments=60)
                )
                for pos in self.controller_screw_positions
            ]
        )

    @property
    def pcb_polygon_2d(self) -> OpenSCADObject:
        return polygon(
            points=[v.as_tuple for v in self._board.left_board.pcb_outline],
        )

    @property
    def switch_cells(self) -> list[SwitchDiodeArrangement]:
        result: list[SwitchDiodeArrangement] = []
        result.extend([cell for col in self._board.left_board.finger.columns for cell in col])
        result.extend(self._board.left_board.thumb.cells)
        return result

    @property
    def switch_holes(self) -> OpenSCADObject:
        return sum(
            [
                translate(cell.position.as_tuple)(
                    square(size=self.c.switch_hole_width, center=True)
                )
                for col in self._board.left_board.finger.columns for cell in col
            ]
            +
            [
                translate(cell.position.as_tuple)(
                    rotate(cell.angle)(
                        square(size=self.c.switch_hole_width, center=True)
                    )
                ) for cell in self._board.left_board.thumb.cells
            ]
        )

    @property
    def wall_shape(self):
        reduced_outline = offset(delta=-wall_width)(
            self.pcb_polygon_2d
        )
        return self.pcb_polygon_2d - reduced_outline

    @property
    def switch_plate_controller_screw_nose_2d(self) -> OpenSCADObject:
        screw = self.controller_screw_positions[2]
        return translate(screw)(
            circle(r=2.7, segments=20)
            +
            back(2.7)(
                left(10)(square((10, 2.7 * 2)))
            )
        )

    @property
    def reset_switch_cutout_2d(self):
        return translate(self._board.left_board.controler_section.reset_switch.pos.as_tuple)(
            square(
                (self.c.reset_switch_cutout_width, self.c.reset_switch_cutout_width),
                center=True
            )
        )

    @property
    def pcb_2d(self) -> OpenSCADObject:
        return (
                self.pcb_polygon_2d
                -
                self.switch_field_screw_holes
                -
                self.controller_field_screw_holes
                -
                translate(self._board.left_board.controler_section.encoder.pos.as_tuple)(circle(d=7.3, segments=60))
        )

    @property
    def pcb_3d(self) -> OpenSCADObject:
        return linear_extrude(self.c.pcb_thickness)(
            self.pcb_2d
        )

    @property
    def bottom_plate_3d(self) -> OpenSCADObject:
        return linear_extrude(self.c.bottom_plate_thickness)(
            self.bottom_plate_2d
        )

    @property
    def usb_plug_entry_point(self) -> tuple[float, float, float]:
        return (
            self._board.left_board.controler_section.promic.pos.x,
            self._board.left_board.inner_upper_corner.y,
            self.z_level_pcb_front
        )

    @property
    def usb_plug(self) -> OpenSCADObject:
        return (
            translate(self.usb_plug_entry_point)(
                scad_part_models.USBPlugCutout()()
            )
        )

    @property
    def jack_entry_point(self) -> tuple[float, float, float]:
        p = self._board.left_board.controler_section.jack.pos
        return (
            p.x,
            p.y,
            self.z_level_pcb_front
        )

    @property
    def jack_open_upwards(self) -> OpenSCADObject:
        return (
            translate(self.jack_entry_point)(
                left(0.1)(scad_part_models.JackCutoutUpwardsOpen()())
            )
        )

    @property
    def jack_open_downwards(self) -> OpenSCADObject:
        return (
            translate(self.jack_entry_point)(
                left(0.1)(scad_part_models.JackCutoutDownwardsOpen()())
            )
        )

    @property
    def encoder_model(self) -> OpenSCADObject:
        return translate(self._board.left_board.controler_section.encoder.pos.as_tuple)(
            rotate((0, 0, 180))(scad_part_models.RotaryEncoder()())
        )

    @property
    def extension_2d(self) -> OpenSCADObject:
        return translate(self._board.left_board.controler_section.encoder.pos.as_tuple)(
            scad_part_models.RotaryEncoder().cutout_2d
        )

    @property
    def switches_model(self) -> OpenSCADObject:
        return sum(
             [
                 translate(cell.position.as_tuple)(
                     scad_part_models.Switch()()
                 )
                 for col in self._board.left_board.finger.columns for cell in col
             ]
             +
             [
                 translate(cell.position.as_tuple)(
                     rotate(cell.angle)(
                         scad_part_models.Switch()()
                     )
                 ) for cell in self._board.left_board.thumb.cells
             ]
         )

    @property
    def promic_model(self) -> OpenSCADObject:
        return translate(self._board.left_board.controler_section.promic.pos.as_tuple)(
            color("red")(scad_part_models.Promic()())
        )

    @property
    def jack_model(self) -> OpenSCADObject:
        return translate(self._board.left_board.controler_section.jack.pos.as_tuple)(
            color("red")(scad_part_models.Jack()())
        )

    @property
    def layout_test(self):
        return rotate((180, 0, 0))(mirror((0, 1, 0))(
            linear_extrude(self.c.SCAD.top_plate_thickness)(
                self.pcb_polygon_2d
                -
                self.switch_holes
                -
                self.switch_field_screw_holes
                -
                self.controller_field_screw_holes
                -
                translate(self._board.left_board.controler_section.encoder.pos.as_tuple)(circle(d=7.3, segments=60))
            )
            +
            down(8.6)(linear_extrude(8.6)(
                self.wall_shape
            ))
        ))

    @property
    def top_plate_2d(self) -> OpenSCADObject:
        c = self.c
        screw_holes = sum([
            translate(pos)(
                circle(d=c.screw_hole_diameter, segments=50)
            )
            for pos in self.switch_screw_positions + self.controller_screw_positions[1:]
        ])
        return (
                self.wall_outline_2d
                -
                offset(delta=-4)(
                    self.wall_outline_2d
                )
                -
                screw_holes
        )

    @property
    def outer_jack_cutout(self):
        return (
            translate(self.jack_entry_point)(
                right(2.6)(
                    up(2.5)(
                        rotate((0, 90, 0))(
                            cylinder(d=8, h=10, segments=128)
                        )
                    )
                )
            )
        )


class OldWallConceptBoard(Board):

    @property
    def controller_cover_plate_2d(self):
        return (
                self.get_controller_section_cover_area_2d(inner_overlap=False)
                -
                self.get_wall_screws_holes()
        )

    @property
    def switch_plate_2d(self) -> OpenSCADObject:
        return (
                self.wall_outline_2d
                +
                self.switch_plate_controller_screw_nose_2d
                -
                self.switch_holes
                -
                self.switch_field_screw_holes
                -
                self.controller_field_screw_holes
        )

    @property
    def bottom_plate_2d(self) -> OpenSCADObject:
        return (
                self.wall_outline_2d
                -
                self.reset_switch_cutout_2d
        )

    @property
    def wall_outline_2d(self) -> OpenSCADObject:
        c = self.c
        return (
            offset(r=c.border_rounding_radius, segments=30)(
                offset(delta=c.outer_wall_width - c.border_rounding_radius)(
                    self.pcb_polygon_2d
                )
            )
        )

    @property
    def outer_wall_2d(self) -> OpenSCADObject:
        c = self.c
        return (
                self.wall_outline_2d
                -
                offset(delta=c.pcb_horizontal_clearance)(
                    self.pcb_polygon_2d
                )
        )

    def get_controller_section_cover_area_2d(self, inner_overlap: bool) -> OpenSCADObject:
        x_1 = (
            self._board.left_board.controler_section.promic.pos.x - m.SCAD.promic_cutout_dimensions[0] / 2
            if inner_overlap else
            self._board.left_board.finger.columns[-1][0].position.x + m.switch_width / 2
        )
        y_1 = self._board.left_board.inner_upper_corner.y - self.c.outer_wall_width
        x_2 = self._board.left_board.inner_upper_corner.x + self.c.outer_wall_width
        y_2 = self._board.left_board.controler_section.encoder.pos.y + 20

        d_x = x_2 - x_1
        d_y = y_2 - y_1

        circle_center = self._board.left_board.thumb.get_radial_point(0, 0)
        circle_radius = self.c.controller_cover_cutout_radius

        if inner_overlap:
            circle_radius += self.c.controller_cover_overlap
            y_2 = self.controller_screw_positions[1][1] - 3
            d_y = y_2 - y_1

        return (
                translate((x_1, y_1))(
                    square((d_x, d_y))
                )
                -
                translate(circle_center.as_tuple)(
                    circle(r=circle_radius, segments=200)
                )
        )



