import enum

from solid import (
    OpenSCADObject,
    offset,
    linear_extrude,
    translate,
    intersection,
    rotate,
    color,
    minkowski,
    square,
    polygon,
    circle,
    mirror
)
from solid.utils import (
    down,
    up,
    back,
    forward,
    right,
    left
)

from terminal_c import scad_part_models, Vector, Polygon, Line
from terminal_c import MagicNumbers as m
from terminal_c import geometry
from terminal_c.configuration.default import Configuration as TermCConfig
from terminal_c.geometry import SwitchDiodeArrangement

from c_house.configuration import AtheneConfiguration
from c_house.lib.board import Board
from c_house import StackedModel, Renderer


class Side(enum.Enum):
    Left = 1
    Right = 2


class SwitchCutout:
    def __init__(self, sw: SwitchDiodeArrangement, c: AtheneConfiguration):
        self.switch_arragement = sw
        self.position = sw.position
        self.c = c
        dim1_v_1 = Vector.from_tuple(c.rim_cutout1_dim) / 2
        dim1_v_2 = Vector(-dim1_v_1.x, dim1_v_1.y)

        self.nw = sw.position + (dim1_v_2).rotate(self.switch_arragement.angle)
        self.se = sw.position - (dim1_v_2).rotate(self.switch_arragement.angle)
        self.ne = sw.position + (dim1_v_1).rotate(self.switch_arragement.angle)
        self.sw = sw.position - (dim1_v_1).rotate(self.switch_arragement.angle)

        self.tongue_cutout = translate(sw.position.as_tuple)(
            rotate(sw.angle)(
                square(c.rim_cutout2_dim, center=True)
            )
        )


class AtheneBoard(Board):

    def __init__(self, configuration: AtheneConfiguration, board: geometry.Board, side: Side):
        Board.__init__(self, configuration, board)
        self.side = side
        self.c = configuration
        self.in_time_calculated_lower_height = None
        self.in_time_calculated_upper_height = None
        print("------------------------------------------------------------------------------------------------------")
        pcb_screw_length = (
            self.c.pcb_thickness + self.c.pcb_switch_plate_distance + self.c.switch_plate_thickness + self.c.nut_height
        )
        print("{:20}: {}".format("PCB-Screw", pcb_screw_length))
        border_screw_length = (
            self.c.nut_height
            +
            self.c.bottom_plate_thickness
            +
            self.lower_wall_height
            +
            self.upper_wall_height
            +
            self.c.switch_plate_thickness
            +
            self.c.rim_plate_thickness
        )
        border_elevated_screw_length = (
            self.c.nut_height
            +
            self.c.bottom_plate_thickness
            +
            self.lower_wall_height
            +
            self.upper_wall_elevated_height
            +
            self.c.cover_plate_thickness
        )
        print("{:20}: {}".format("Border-Screw", border_screw_length))
        print("{:20}: {}".format("Elev. Border-Screw", border_elevated_screw_length))
        print("{:20}: {}".format("Lower wall height", self.lower_wall_height))
        print("{:20}: {}".format("Upper wall height", self.upper_wall_height))
        print("{:20}: {}".format("Elevated wall height", self.upper_wall_height + self.c.controller_cover_elevation))
        print("------------------------------------------------------------------------------------------------------")

        self.rim_switch_cutouts = [SwitchCutout(sw, c) for sw in self.switch_cells]

    @property
    def middle_thumb_angle(self):
        return (
            self._board.left_board.c.thumb_row2_radial_positions[1]
            -
            self._board.left_board.c.thumb_row2_radial_positions[0]
        )

    @property
    def wall_screw_positions(self) -> list[Vector]:
        o = self.extended_pcb_outline
        d = self.c.screw_distance
        usb = self.usb_plug_entry_point[:2]
        jack = Vector.from_tuple(self.jack_entry_point[:2])

        # preparation inner thumb screw
        inner_thumb_cluster_screw_translation_vector = (o[21] - o[20]).rotate(90)
        inner_thumb_cluster_screw_translation_vector /= inner_thumb_cluster_screw_translation_vector.length

        return [
            # outer border
            o[0] + Vector(c.extra_outer_wall_width__outer - 2.7, 3),
            o[-1] + Vector(c.extra_outer_wall_width__outer - 2.7, -3),
            # upper finger
            o[4]+ Vector(c.extra_outer_wall_width__outer - 2.9, c.extra_outer_wall_width__outer - 2.9),
            o[11] + Vector(-c.extra_outer_wall_width__outer + 2.9, c.extra_outer_wall_width__outer - 2.9),
            # controller back
            o[13] + Vector(16, 0.9),
            o[13] + Vector(21.8, 0.9),
            # inner border, corners
            o[15] + Vector(-c.extra_outer_wall_width__inner + 2.7, -1.5),
            o[16] + Vector(-c.extra_outer_wall_width__inner + 2.7, -11.5),
            # inner border, jack
            Vector(o[15].x-c.extra_outer_wall_width__inner + 2.7, jack.y - 6),
            # thumb cluster, outer-middle-inner
            o[17] + Vector(-2, 1.1),
            ((o[21] + o[22]) / 2) + Vector(0, -1.0).rotate(self.middle_thumb_angle),
            (o[24] + o[25]) / 2 + inner_thumb_cluster_screw_translation_vector * 1.1,


            # o[3] + Vector(-d, -(o[4] - o[3]).length / 1.5),
            # o[6] + (o[7] - o[6])/2.0 + Vector(0, -d),
            # o[10] + Vector(+d * 1.2, -d),
            # o[11] + Vector(0, -d),
            # jack + Vector(d, 7),
            # o[13] + Vector(0, -d).rotate(self._board.left_board.thumb.cells[-1].angle),
            # o[16] + Vector(0, d).rotate(self._board.left_board.thumb.cells[1].angle),
            # o[19] + Vector(0, d).rotate(self._board.left_board.thumb.cells[0].angle),
            # o[24] + ((o[23]-o[24])/2) + Vector(-d, 0).rotate((o[23]-o[24]).angle),
            # o[25] + ((o[26]-o[25])/1) + Vector(d, 0).rotate((o[23]-o[24]).angle),
            # o[30] + Vector(0, +d),
        ]

    def get_wall_screws_holes(self, extended_diameter: bool = False) -> OpenSCADObject:
        diameter = (
            self.c.screw_hole_diameter_extended
            if extended_diameter else
            self.c.screw_hole_diameter
        )
        return sum([
            translate(p.as_tuple)(
                circle(d=diameter, segments=self.c.screw_hole_segments)
            )
            for p in self.wall_screw_positions
        ])

    @property
    def pcb_outline(self) -> Polygon:
        """
        The exact outline polygon of the PCB.
        """
        return self._board.left_board.pcb_outline.copy()

    @property
    def pcb_outline_w_clearance_2d(self) -> OpenSCADObject:
        """
        The outline polygon of the PCB with vertical clearance for the walls.
        """
        return (
            offset(delta=self.c.pcb_horizontal_clearance)(
                polygon(
                    points=[v.as_tuple for v in self.pcb_outline],
                )
            )
        )

    @property
    def wall_cutout_2d(self) -> OpenSCADObject:
        """
        The polygon cut out from the wall elements.
        It's a smaller version of the PCB, so that the PCB can still sit on the wall.
        """
        return (
            offset(delta=-self.c.inner_wall_width)(
                polygon(
                    points=[v.as_tuple for v in self.pcb_outline],
                )
            )
        )

    @property
    def extended_pcb_outline(self) -> Polygon:
        """
        The extended outline of the PCB.
        The extension are additional areas to take the wall screws.
        """
        base = self._board.left_board.pcb_outline.copy()

        # extra inner border
        base[0] += Vector(-c.extra_outer_wall_width__outer, 0)
        base[-1] += Vector(-c.extra_outer_wall_width__outer, 0)

        # 2nd finger col
        base.insert(4, base[3] + Vector(0, -c.extra_outer_wall_width__outer))
        base.insert(4, base[3] + Vector(-c.extra_outer_wall_width__outer, -c.extra_outer_wall_width__outer))
        base.insert(4, base[3] + Vector(-c.extra_outer_wall_width__outer, 0))
        del(base[3])

        # 5th finger col
        p = base[10]
        base.insert(10, p + Vector(0, -c.extra_outer_wall_width__outer))
        base.insert(11, p + Vector(c.extra_outer_wall_width__outer, -c.extra_outer_wall_width__outer))
        base.insert(12, p + Vector(c.extra_outer_wall_width__outer, -1.6))
        del(base[13])
        base[13] += Vector(0, -1.6)

        # extra outer border
        outer_border_line = Line(base[15], base[16])
        upper_thumb_line = Line(base[16], base[17])
        outer_border_line = outer_border_line.translate(Vector(c.extra_outer_wall_width__inner, 0))
        base[15] += Vector(c.extra_outer_wall_width__inner, 0)
        base[16] = outer_border_line.intersection_with(upper_thumb_line)

        # controller backside (USB-port)
        base[14] = Vector(base[15].x, base[13].y)

        # thumb cluster outer
        upper_thumb_vector = upper_thumb_line.vector
        translation_vector = upper_thumb_vector * c.extra_outer_wall_width__outer / upper_thumb_vector.length
        base[17] += translation_vector
        base[18] += translation_vector
        base[19] += translation_vector

        # thumb cluster lower triangle
        translation_vector = Vector(0, 1).rotate(self.middle_thumb_angle)
        base[20] += translation_vector
        base[21] += translation_vector * 4  #c.extra_outer_wall_width__outer
        base[22] += translation_vector * 4  #c.extra_outer_wall_width__outer
        base[23] += translation_vector

        # thumb cluster inner
        translation_vector = Vector(-c.extra_outer_wall_width__outer, 0).rotate(self._board.left_board.c.thumb_row1_radial_positions[0])
        del(base[24])
        del(base[24])
        del(base[25])
        base[24] += translation_vector
        base[25] += translation_vector
        p = Line(base[24], base[25]).intersection_with(Line(base[26], base[26] + Vector(-1, 0)))
        base[25] = p

        return base

    @property
    def wall_outline_2d(self) -> OpenSCADObject:
        c = self.c
        return (
            offset(r=c.border_rounding_radius, segments=30)(
                offset(delta=c.outer_wall_width - c.border_rounding_radius)(
                    polygon(
                        points=[v.as_tuple for v in self.extended_pcb_outline],
                    )
                )
            )
        )

    @property
    def bottom_plate_2d(self) -> OpenSCADObject:
        return (
            self.wall_outline_2d
            -
            self.get_wall_screws_holes(extended_diameter=False)
            -
            self.reset_switch_cutout_2d
        )

    @property
    def controller_cover_plate_2d(self):
        return (
            self.upper_wall_elevation_area_2d
            -
            self.get_wall_screws_holes(extended_diameter=False)
        )

    @property
    def controller_cover_plate_holed_2d(self):
        return (
                self.upper_wall_elevation_area_2d
                -
                self.get_wall_screws_holes(extended_diameter=False)
                -
                translate(self._board.left_board.controler_section.encoder.pos.as_tuple)(
                    circle(d=c.encoder_shaft_hole_diameter, segments=120)
                )
        )

    @property
    def _upper_plate_base_2d(self) -> OpenSCADObject:
        return (
                self.wall_outline_2d
                -
                self.get_wall_screws_holes(extended_diameter=False)
                -
                offset(delta=0.2)(self.upper_wall_elevation_area_2d)
                -
                self.switch_field_screw_holes
                -
                self.controller_field_screw_holes
        )

    @property
    def switch_plate_2d(self) -> OpenSCADObject:
        return (
            self._upper_plate_base_2d
            -
            self.switch_holes
        )

    def _get_finger_rim_cutout(self, start_ix: int) -> OpenSCADObject:
        s1 = self.rim_switch_cutouts[start_ix]
        s2 = self.rim_switch_cutouts[start_ix + 3]
        s3 = self.rim_switch_cutouts[start_ix + 5]
        s4 = self.rim_switch_cutouts[start_ix + 2]
        return polygon([p.as_tuple for p in [
            s1.nw,
            s1.sw,
            s1.se,
            s2.sw,
            s2.se,
            s3.ne,
            s3.nw,
            s4.ne,
            s4.nw
        ]])

    def _get_thumb_rim_cutout(self):
        s = self.rim_switch_cutouts
        return polygon([p.as_tuple for p in [
            s[-1].se,
            s[-1].sw,
            s[-2].se,
            s[-2].sw,
            s[-3].se,
            s[-3].sw,
            s[-4].se,
            s[-4].sw,
            s[-4].nw,
            s[-6].sw,
            s[-6].nw,
            s[-6].ne,
            s[-5].nw,
            s[-5].ne,
            s[-5].se,
            s[-1].ne,
        ]])

    @property
    def rim_plate_2d(self) -> OpenSCADObject:
        return (
            self._upper_plate_base_2d
            -
            self._get_finger_rim_cutout(0)
            -
            self._get_finger_rim_cutout(6)
            -
            self._get_finger_rim_cutout(12)
            -
            self._get_thumb_rim_cutout()
            -
            sum(s.tongue_cutout for s in self.rim_switch_cutouts)
        )

    @property
    def lower_wall_height(self) -> float:
        c = self.c
        return c.pcb_bottom_plate_distance + c.pcb_thickness + c.pcb_vertical_clearance

    @property
    def lower_wall(self) -> OpenSCADObject:
        c = self.c
        wall_outline = self.wall_outline_2d
        pcb = self.pcb_outline_w_clearance_2d
        cutout = self.wall_cutout_2d
        outer_part = wall_outline - pcb
        inner_part = pcb - cutout
        switch_section = offset(r=0.2, segments=32)(scad_part_models.SwitchSectionPCBParts2D().get())
        if self.side == Side.Right:
            switch_section = mirror((1, 0, 0))(switch_section)
        return (
                linear_extrude(c.pcb_bottom_plate_distance)(
                    inner_part
                )
                +
                linear_extrude(self.lower_wall_height)(
                    outer_part
                )
                -
                (
                    # cut out the sections for PCB parts (socket, diode and LEC per switch, and reset button)
                    up(c.pcb_bottom_plate_distance - 2.6 + 0.01)(
                        linear_extrude(2.6)(
                            sum([
                                translate(cell.position.as_tuple)(
                                    rotate(180 + cell.angle)(
                                        switch_section
                                    )
                                )
                                for cell in self.switch_cells
                            ])
                            +
                            translate(self._board.left_board.controler_section.reset_switch.pos.as_tuple)(
                                square(
                                    (6.4, 10),
                                    center=True
                                )
                            )

                        )
                    )
                )
                -
                (
                    # cut out the circles for the two nuts on PCB boarder of the controller section
                    up(c.pcb_bottom_plate_distance - 2.8 + 0.01)(
                        linear_extrude(2.8)(
                            sum([
                                translate(pos)(
                                    circle(d=self.c.nut_hull_circle_d + 0.4, segments=50)
                                )
                                for pos in self.controller_screw_positions[:2]
                            ])
                        )
                    )
                )
                -
                down(c.bottom_plate_thickness + c.pcb_vertical_clearance)(
                    self.usb_plug
                    +
                    self.jack_open_upwards
                    +
                    self.outer_jack_cutout
                )
                -
                down(1)(
                    linear_extrude(50)(
                        self.get_wall_screws_holes(extended_diameter=True)
                    )
                )
        )

    @property
    def upper_wall_height(self) -> float:
        return self.c.pcb_switch_plate_distance

    @property
    def upper_wall_elevated_height(self) -> float:
        return self.upper_wall_height + self.c.controller_cover_elevation

    @property
    def upper_wall(self) -> OpenSCADObject:
        c = self.c
        switch_holes = sum([
            translate(cell.position.as_tuple)(
                rotate(cell.angle)(
                    square((15, 16.5), center=True)
                )
            )
            for cell in self.switch_cells
        ])
        screw_holes = sum([
            translate(pos)(
                circle(d=c.screw_hole_diameter_extended, segments=50)
            )
            for pos in self.switch_screw_positions + self.controller_screw_positions
        ])
        return (
                linear_extrude(self.upper_wall_height)(
                    self.wall_outline_2d
                    -
                    self.controller_section_cutout_2d(False)
                    -
                    switch_holes
                    -
                    screw_holes
                )
                +
                (
                    linear_extrude(self.upper_wall_height + c.switch_plate_thickness)(
                        self.upper_wall_elevation_area_2d
                        -
                        self.controller_section_cutout_2d(False)
                        -
                        screw_holes
                    )
                )
                +
                (
                    linear_extrude(self.upper_wall_elevated_height)(
                        self.upper_wall_elevation_area_2d
                        -
                        self.controller_section_cutout_2d(True)
                        -
                        screw_holes
                    )
                )
                -
                down(
                    c.bottom_plate_thickness
                    +
                    c.pcb_bottom_plate_distance
                    +
                    c.pcb_thickness
                )(
                    self.usb_plug
                    +
                    self.jack_open_downwards
                    +
                    self.outer_jack_cutout
                )
                -
                down(10)(
                    linear_extrude(50)(
                        self.get_wall_screws_holes(extended_diameter=True)
                    )
                )
        )

    def controller_section_cutout_2d(self, with_pcb_screws_cut_out: bool) -> OpenSCADObject:
        corner_screw_nose = (
            translate(self.controller_screw_positions[0])(
                back(100 - 4)(minkowski()(
                    square(100),
                    circle(d=c.screw_head_d + 0.8, segments=20)
                ))
            )
        )

        x_1 = (
            self._board.left_board.controler_section.promic.pos.x - m.SCAD.promic_cutout_dimensions[0] / 2
        )
        y_1 = self._board.left_board.inner_upper_corner.y + 1.8
        x_2 = self._board.left_board.inner_upper_corner.x
        y_2 = self.controller_screw_positions[1][1] - (0 if with_pcb_screws_cut_out else 3 )
        d_y = y_2 - y_1
        d_x = x_2 - x_1

        circle_center = self._board.left_board.thumb.get_radial_point(0, 0)
        circle_radius = self.c.controller_cover_cutout_radius + self.c.controller_cover_overlap

        result = (
            translate((x_1, y_1, 0))(
                square((d_x, d_y))
            )
            -
            translate(circle_center.as_tuple)(
                circle(r=circle_radius, segments=200)
            )
        )

        if with_pcb_screws_cut_out:
            return (
                result
                +
                translate(self.controller_screw_positions[1])(circle(r=2.5, segments=50))
            )
        else:
            return (
                result
                -
                self.switch_plate_controller_screw_nose_2d
                -
                corner_screw_nose
            )

    @property
    def upper_wall_elevation_area_2d(self):
        x_1 = self._board.left_board.finger.columns[-1][0].position.x + m.switch_width / 2
        y_1 = self._board.left_board.inner_upper_corner.y - 60
        circle_center = self._board.left_board.thumb.get_radial_point(0, 0)
        circle_radius = self.c.controller_cover_cutout_radius
        return (
            intersection()(
                (
                    translate((x_1, y_1, 0))(
                        square(140)
                    )
                    -
                    translate(circle_center.as_tuple)(
                        circle(r=circle_radius, segments=200)
                    )
                ),
                self.wall_outline_2d
            )
        )

    @property
    def model(self) -> OpenSCADObject:
        c = self.c
        z = 0
        return StackedModel([
            #(self.outer_jack_cutout, z, "Lime"),
            (self.bottom_plate_3d, z, "DodgerBlue"),
            (self.lower_wall, z := z + c.bottom_plate_thickness, "Grey"),
            (self.pcb_3d, z := z + c.pcb_bottom_plate_distance, "orange"),
            (self.upper_wall, z := z + c.pcb_thickness, "blue"),
            #(self.switches_model, z, "red"),
            #(self.promic_model, z, "red"),
            #(self.jack_model, z, "red"),
            #(self.encoder_model, z, "red"),
            (self.switch_plate_3d, z := z + c.pcb_switch_plate_distance, "Grey"),
            (self.rim_plate_3d, z := z + c.switch_plate_thickness, "Purple"),
            #(self.controller_cover_plate_3d, z := z + c.controller_cover_elevation, "hotpink"),

            #(self.top_plate_3d, z := z + c.switch_plate_top_plate_distance + c.switch_plate_thickness, "hotpink"),
            #(self.controller_cover_plate_3d, z := z + c.top_plate_thickness + c.top_plate_cover_plate_distance, "green")
        ])(strech=1)

    @property
    def controller_cover_plate_3d(self) -> OpenSCADObject:
        return linear_extrude(self.c.cover_plate_thickness)(
            self.controller_cover_plate_2d
        )

    @property
    def controller_cover_plate_holed_3d(self) -> OpenSCADObject:
        return linear_extrude(self.c.cover_plate_thickness)(
            self.controller_cover_plate_holed_2d
        )

    @property
    def switch_plate_3d(self) -> OpenSCADObject:
        return color("grey")(linear_extrude(self.c.switch_plate_thickness)(
            self.switch_plate_2d
        ))

    @property
    def rim_plate_3d(self) -> OpenSCADObject:
        return color("purple")(linear_extrude(self.c.rim_plate_thickness)(
            self.rim_plate_2d
        ))

    @property
    def controller_plate_holder(self) -> OpenSCADObject:
        wall_height = 1.4
        bottom_height = 1
        clearance = 0.15
        wall_width = 1.6
        hole_diameter = 1.8
        plate = self.upper_wall_elevation_area_2d
        plate_plus_clearance = offset(delta=clearance, chamfer=True)(plate)
        plate_plus_wall = offset(r=wall_width)(plate_plus_clearance)
        hole = translate(self._board.left_board.controler_section.encoder.pos.as_tuple)(
            circle(d=hole_diameter, segments=60)
        )

        result = (
            linear_extrude(bottom_height)(
                plate_plus_wall
                -
                hole
            )
            +
            linear_extrude(wall_height + bottom_height)(
                plate_plus_wall - plate_plus_clearance
            )
        )

        return left(150)(result)


if __name__ == "__main__":
    c = AtheneConfiguration()
    pcb_board = geometry.Board(TermCConfig())

    for side in [Side.Left, Side.Right]:
        board = AtheneBoard(c, pcb_board, side=side)
        r = Renderer("athene/{}".format(side.name), flip=(side == Side.Left))
        side_letter = "r" if side == Side.Right else "l"

        r(side_letter + "_controller_plate_holder", board.controller_plate_holder)
        r(side_letter + "_bottom_plate_2d", board.bottom_plate_2d)
        r(side_letter + "_bottom_plate_3d", board.bottom_plate_3d)
        r(side_letter + "_switch_plate_2d", board.switch_plate_2d)
        r(side_letter + "_switch_plate_3d", board.switch_plate_3d)
        r(side_letter + "_rim_plate_2d", board.rim_plate_2d)
        r(side_letter + "_rim_plate_3d", board.rim_plate_3d)
        r(side_letter + "_controller_plate_2d", board.controller_cover_plate_2d)
        r(side_letter + "_controller_plate_3d", board.controller_cover_plate_3d)
        r(side_letter + "_lower_wall", board.lower_wall)
        r(side_letter + "_upper_wall", board.upper_wall)
        r(side_letter + "_switch_pcb_parts", scad_part_models.SwitchSectionPCBParts2D().get())
        r(side_letter + "_jack_cutout_upper_wall", scad_part_models.JackCutoutDownwardsOpen()())
        #r("switch_plate_2d", board.switch_plate_2d)
        #r("test_outer_wall_2d", board.outer_wall_2d)
        #r("switch_plate_3d", board.switch_plate_3d)
        #r("bottom_plate_3d", board.bottom_plate_3d)
        r(side_letter + "model", board.model)

    board = AtheneBoard(c, pcb_board, side=Side.Left)

    r = Renderer("athene/both", flip=False)
    r(
        "bottom",
        forward(4)(board.bottom_plate_2d)
        +
        back(4)(mirror((0, 1, 0))(board.bottom_plate_2d))
        +
        right(59)(square((1, 4), center=True))
        +
        right(78.5)(square((1, 4), center=True))
    )
    r(
        "rim",
        forward(4)(board.rim_plate_2d)
        +
        back(4)(mirror((0, 1, 0))(board.rim_plate_2d))
        +
        right(59)(square((1, 4), center=True))
        +
        right(78.5)(square((1, 4), center=True))
    )
    r(
        "switch",
        forward(4)(board.switch_plate_2d)
        +
        back(4)(mirror((0, 1, 0))(board.switch_plate_2d))
        +
        right(59)(square((1, 4), center=True))
        +
        right(78.5)(square((1, 4), center=True))
    )
    r(
        "controller",
        left(117)(board.controller_cover_plate_2d)
        +
        right(117)(mirror((1,0,0))(board.controller_cover_plate_2d))
        +
        forward(3)(square((3, 1), center=True))
        +
        forward(69)(square((3, 1), center=True))
    )
