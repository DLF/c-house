# C-House... a case for the Teminal-C split keyboard

Seems we need screws of length...
* 8mm for the pcb
* 12mm for the wall
* 16mm for the elevated wall

Head diameter for M2:
* DIN 7985: 4mm
* ISO 1207: 3.8mm
* ISO 7045: 4mm
* ISO 7046: 3.8
* DIN 84: 4mm

# ToDo
* [x] fix screw length calculation
* [x] much higher indent for usb plug
